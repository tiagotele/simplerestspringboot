package hello.model;

public class MyModel {
	private long id;
	private String firstName;
	private String lastName;

	public MyModel() {
		id = 0;
		lastName = "";
	}

	public MyModel(long id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	@Override
	public String toString() {

		return "{id=" + id + "firstName=" + lastName + "}";
	}
}
