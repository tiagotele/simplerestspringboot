package hello.rest;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hello.model.MyModel;

@RestController
public class MyModelRest {

	private final AtomicLong counter = new AtomicLong();

	@RequestMapping("/")
	public String myEndpointWorking() {
		return "Working!";
	}

	@RequestMapping("/myendpoint")
	public MyModel myEndpointGet(@RequestParam(value = "firstName", defaultValue = "Hello") String firstName,
			@RequestParam(value = "lastName", defaultValue = "World") String lastName) {
		System.out.println(counter);
		return new MyModel(counter.incrementAndGet(), firstName, lastName);
	}

	@RequestMapping(value = "/myendpoint", method = RequestMethod.POST)
	public MyModel myEndpointPost(@RequestBody final MyModel myModel) {
		System.out.println(counter);
		myModel.setId(counter.incrementAndGet());
		return myModel;
	}
}