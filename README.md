# README #

Simple Rest project using Spring boot.

### How do I get set up? ###

* Into folder root do
 mvn spring-boot:run

* In a terminal run 
For GET curl http://localhost:8080/myendpoint
For POST curl  -H "Content-Type: application/json" -d '{"firstName":"Tiago","lastName":"Melo"}' "http://localhost:8080/myendpoint";